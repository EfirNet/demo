const express = require('express');
const nodemailer = require('nodemailer');
const bodyParser = require('body-parser');

require('dotenv').config();




const app = express();
const port = 3000;
const { FROM_EMAIL, PASSWORD } = process.env;
const transporter = nodemailer.createTransport({
  host: "smtp.meta.ua",
  port: 465,
  secure: true,
  auth: {
    user: FROM_EMAIL,
    pass: PASSWORD
  },
});

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.post('/', (req, res) => {
  const { name, email, message } = req.body;

  const mailOptions = {
    from: FROM_EMAIL,
    to: 'dreaming.team.dev@gmail.com',
    subject: `New message from ${name}`,
    text: `${message} \n\n Sender name: ${name} \n Sender email: ${email}`
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      res.status(500).send('Error sending message');
    } else {
      console.log('Message sent: ' + info.response);
      res.send('Message sent');
    }
  });
});

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
